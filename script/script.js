
/*Exercise 1*/
/*1. В мові джаваскріпт все побудовано на прототипах. Якщо ми хочемо створити новий об'єкт,
 то для цього джава скрипт використовує прототип об'єкту і на його основі створює новий об'єкт.
 Прототип має свої власивості та методи, які при створенні об'єктів на його основі наслідуються вже новим об'єктом.
   Коли ми шукаємо властивість або метод у об'єкта, рушій спочатку шукає їх безпосередньо у об'єкта,
  а якщо їх немає  то переходить до прототипу, за яким був створений об'єкт*/
/*2. super() потрібно викликати, щоб викликати метод constructor з батьківського класу.
 Це потрібно для того, щоб для this створився новий об'єкт, бо конструктор нащадка не створює новий об'єкт при роботі конструктора зі словом new */


class Employee {
    constructor(name, age, salary) {
        this._name = name
        this._age = age
        this._salary = salary
    }

    get name() {
        return this._name
    }

    set name(value) {
        if (value.length !== 0) {
            return this._name = value
        }
    }

    get age() {
        return this._age
    }

    set age(value) {
        if (typeof value === 'number') {
            return this._age = value
        }
    }

    get salary() {
        this._salary *= 3
        return this._salary
    }

    set salary(value) {
        if (typeof value === 'number') {
            return this._salary = value
        }
    }
}


class Programmer extends Employee {
    constructor(name, age, salary) {
        super(name, age, salary)
    }

    set salary(value) {
         if (typeof value === 'number') {
             return this._salary = value
         }
    }

    get salary() {
        return this._salary * 3
    }
}

let person1 = new Programmer()
person1.name = 'Dan'
person1.age = 38
person1.salary = 1000
console.log(person1)
console.log(person1.salary)


let person2 = new Programmer()
person2.name = 'Brie'
person2.age = 26
person2.salary = 500
console.log(person2)
console.log(person2.salary)


let person3 = new Programmer()
person3.name = 'Sarah'
person3.age = 31
person3.salary = 800
console.log(person3)
console.log(person3.salary)




